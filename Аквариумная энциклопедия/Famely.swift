//
//  Famely.swift
//  Аквариумная энциклопедия
//
//  Created by Stepanov Nickolay on 29.11.15.
//  Copyright © 2015 Stepanov Nickolay. All rights reserved.
//

import Foundation
import UIKit

class Famely{
    var name:String
    var description:String
    var image: UIImage
    init(named:String,description:String,imageName:String){
        self.name = named
        self.description = description
        if let img = UIImage(named:imageName){
            image = img
        }else{
            image = UIImage(named: "no_photo")!
        }
    }
}