//
//  Famelies.swift
//  Аквариумная энциклопедия
//
//  Created by Stepanov Nickolay on 29.11.15.
//  Copyright © 2015 Stepanov Nickolay. All rights reserved.
//

import Foundation
import UIKit
class Famelies
{
    var name:String
    var famelies :[Famely]
    var description:String
    var image: UIImage
    init(named:String,includeFamelies:[Famely],Description:String,imageName:String){
        name = named
        famelies = includeFamelies
        description = Description
        if let img = UIImage(named:imageName){
            image = img
        }else{
            image = UIImage(named: "no_photo")!
        }
    }
    class func FameliesLine() -> [Famelies]
    {
        return [self.Argusovie(),self.Badisovie(),self.Brizgunovie(),self.Carpozub(),self.Chobotn(),self.Cichlid(),self.Laberintovie(),self.Tetras(),self.Vuenovie()]
    }
    
    fileprivate class func Argusovie() -> Famelies
    {
        var fishes = [Famely]()
        fishes.append(Famely(named: "Неон",description: "небольшая яркая рыбка",imageName: "neon_red"))
        return Famelies(named: "Аргусовые", includeFamelies: fishes,Description:"",imageName: "argusovie")
    }
    
    fileprivate class func Badisovie() -> Famelies
    {
        var fishes = [Famely]()
        fishes.append(Famely(named: "Неон",description: "небольшая яркая рыбка",imageName: "neon_red"))
        return Famelies(named: "Бадисовые", includeFamelies: fishes,Description:"",imageName: "badisovie")
    }
    
    fileprivate class func  Brizgunovie() -> Famelies
    {
        var fishes = [Famely]()
        fishes.append(Famely(named: "Неон",description: "небольшая яркая рыбка",imageName: "neon_red"))
        return Famelies(named: "Брызгуновые", includeFamelies: fishes,Description:"",imageName: "brizgunovie")
    }
    
    fileprivate class func  Carpozub() -> Famelies
    {
        var fishes = [Famely]()
        fishes.append(Famely(named: "Неон",description: "небольшая яркая рыбка",imageName: "neon_red"))
        return Famelies(named: "Карпозубые", includeFamelies: fishes,Description:"",imageName: "carpozub")
    }
    
    fileprivate class func  Chobotn() -> Famelies
    {
        var fishes = [Famely]()
        fishes.append(Famely(named: "Неон",description: "небольшая яркая рыбка",imageName: "neon_red"))
        return Famelies(named: "Хоботнорылые", includeFamelies: fishes,Description:"",imageName: "chobotn")
    }
    
    fileprivate class func Cichlid() -> Famelies
    {
        var fishes = [Famely]()
        fishes.append(Famely(named: "Дискус",description: "король аквариума",imageName: "discus"))
        fishes.append(Famely(named: "Скалярия",description: "королева аквариума",imageName: "angel_fish"))
        fishes.append(Famely(named: "Неон",description: "небольшая яркая рыбка",imageName: "neon_red"))
        fishes.append(Famely(named: "Неон jfsfs fsefsefsefsefesfse fsf",description: "небольшая яркая рыбка",imageName: "neon_red"))
        return Famelies(named: "Цихлиды", includeFamelies: fishes,Description:"рыбы с интеллектом",imageName: "cichlids")
    }
    fileprivate class func  Laberintovie() -> Famelies
    {
        var fishes = [Famely]()
        fishes.append(Famely(named: "Неон",description: "небольшая яркая рыбка",imageName: "neon_red"))
        return Famelies(named: "Лабиринтовые", includeFamelies: fishes,Description:"",imageName: "laberintovie")
    }

    fileprivate class func Tetras() -> Famelies
    {
        var fishes = [Famely]()
        fishes.append(Famely(named: "Неон",description: "небольшая яркая рыбка",imageName: "neon_red"))
        fishes.append(Famely(named: "Терненция",description: "",imageName: "terneciyablue"))
        return Famelies(named: "Харациновые", includeFamelies: fishes,Description:"Амазонские блестяшки",imageName: "tetras")
    }
    
    fileprivate class func Vuenovie() -> Famelies
    {
        var fishes = [Famely]()
        fishes.append(Famely(named: "Неон",description: "небольшая яркая рыбка",imageName: "neon_red"))
        return Famelies(named: "Вьюновые", includeFamelies: fishes,Description:"",imageName: "vuenovie")
    }
    
    
    
}
