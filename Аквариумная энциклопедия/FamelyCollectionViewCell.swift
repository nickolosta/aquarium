//
//  FishCollectionViewCell.swift
//  Аквариумная энциклопедия
//
//  Created by Stepanov Nickolay on 01.12.15.
//  Copyright © 2015 Stepanov Nickolay. All rights reserved.
//

import UIKit

class FamelyCollectionViewCell: UICollectionViewCell {    
    @IBOutlet weak var famely_img: UIImageView!
    @IBOutlet weak var famely_name: UITextView!
}

